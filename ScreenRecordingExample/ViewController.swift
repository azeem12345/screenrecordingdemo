//
//  ViewController.swift
//  ScreenRecordingExample
//
//  Created by Azeem Ahmed on 3/23/20.
//  Copyright © 2020 Azeem Ahmed. All rights reserved.
//

import UIKit
import ReplayKit
import Photos

class ViewController: UIViewController {

    @IBOutlet weak var viewForScreenRecording: UIView!
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        recordScreen()
        
        
//         let fetchOptions = PHFetchOptions()
//                               fetchOptions.sortDescriptors = [NSSortDescriptor(key: "creationDate", ascending: true)]

        // After uploading we fetch the PHAsset for most recent video and then get its current location url

//        let fetchResult = PHAsset.fetchAssets(with: .video, options: fetchOptions).lastObject
//                               PHImageManager().requestAVAsset(forVideo: fetchResult!, options: nil, resultHandler: { (avurlAsset, audioMix, dict) in
//                                   //let newObj = avurlAsset as! AVURLAsset
//                                   //print(newObj.url)
//
//                                   })
//
    }
    
    func recordScreen() {
        let broadcastPicker = RPSystemBroadcastPickerView(frame: CGRect(x: 0, y: 0, width: 50, height: 50))
        broadcastPicker.backgroundColor = UIColor.green
        broadcastPicker.showsMicrophoneButton = true
        broadcastPicker.layer.cornerRadius = broadcastPicker.frame.height/2
        
        for subview  in broadcastPicker.subviews {
            let b = subview as! UIButton
            b.setImage(UIImage(named : "record.png"), for: .normal)
        }
        
        viewForScreenRecording.addSubview(broadcastPicker)
    }
    
//    func deleteScreenRecordVideoFromGallery(){
//
//        let fileManager = FileManager.default
//        let documentsUrl =  FileManager.default.urls(for: .documentDirectory, in: .userDomainMask).first! as NSURL
//
//        do {
//            let filePaths = try fileManager.contentsOfDirectory(atPath: "\(documentsUrl)")
//            for filePath in filePaths {
//                try fileManager.removeItem(atPath: NSTemporaryDirectory() + filePath)
//            }
//        } catch {
//            print("Could not clear temp folder: \(error)")
//        }
//    }
    
}

